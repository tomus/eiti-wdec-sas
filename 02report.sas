proc sql;
create table temp as
	select k.*, prev.close as prev_close
	from p.kursy k left join p.kursy prev on k.date = prev.date + 7
	where k.name = prev.name or prev.name is null;
quit;

data reportData (keep=name close vol date week_change);
	set temp;
	week_change = (close-prev_close)/prev_close;
	where YEAR(date) = 2007;
run;

proc sort data=reportData out=reportData;
   by name date;
run;

proc means data = reportData noprint;
	by name;
	var close;
	output out = p.price_stats (drop = _TYPE_ _FREQ_)  min = price_min max = price_max mean = price_mean;
run;

proc means data = reportData noprint;
	by name;
	var week_change;
	output out = p.change_stats (drop = _TYPE_ _FREQ_) mean = change_mean var = change_var;
run;

data temp2;
	merge p.price_stats p.change_stats;
	by name;
run;

proc report data = temp2;
	title 'Statystyki notowa� w roku 2007';
	define name / 'Sp�ka (nazwa)';
	define price_max / 'Min. kurs';
	define price_min / 'Maks. kurs';
	define price_mean / '�r. kurs';
	define change_mean / '�r. zwrot tygodniowy';
	define change_var / 'Wariancja�zwrot�w tygodniowych';
run;
