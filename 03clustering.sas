proc cluster method = median data = p.change_stats outtree = change_tree;
	id name;
	var change_mean;
run;

proc tree nclusters = 2 data = change_tree out = change_mean_cluster(rename=(_name_=name) rename=(cluster=mean_cluster) drop=CLUSNAME) noprint;
run;

proc sort data = change_mean_cluster;
	by name;
run;

proc cluster method = median data = p.change_stats outtree = change_tree;
	id name;
	var change_var;
run;

proc tree nclusters = 2 data = change_tree out = change_var_cluster(rename=(_name_=name) rename=(cluster=var_cluster) drop=CLUSNAME) noprint;
run;

proc format;
value cluster_desc
			1='ma�y��r.�zwrot,�ma�a�wariancja'
			2='du�y��r.�zwrot,�ma�a�wariancja'
			3='ma�y��r.�zwrot,�du�a�wariancja'
			4='du�y��r.�zwrot,�du�a�wariancja';
run;

proc sort data = change_var_cluster;
	by name;
run;

data p.clustered_shares (keep=name cluster);
	merge change_mean_cluster change_var_cluster;
	by name;
	format cluster cluster_desc.;
	if mean_cluster = 1 then
		cluster = var_cluster;
	else
		cluster = var_cluster+2;
run;

data p.plot_data;
	merge p.clustered_shares p.change_stats;
	by name;
run;

proc sort data = p.plot_data;
	by cluster;
run;

proc gplot data=p.plot_data;
	plot change_var*change_mean=cluster / legend;
	title 'Grupy sp�ek';
run;
