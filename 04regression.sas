%let user_date = 02/06/2008;
%window user_input
"Please enter date [DD/MM/YYYY]:" user_date attr=underline required=no;
%display user_input;

proc sql;
create table temp as
	select name, date, close
	from p.kursy
	union
	select distinct name, input("&user_date", DDMMYY10.), input('', 2.) as close from p.kursy;
quit;

proc reg data = temp;
	by name;
	model close = date / p;
	output out = p.regression p=result;
run;

data temp2(drop = close);
	set p.regression;
	where date = input("&user_date", DDMMYY10.);
run;

proc report data = temp2;
	title 'Przewidywane ceny';
	define name / 'Sp�ka (nazwa)';
	define date / 'Data';
	define result / 'Cena';
run;
