libname p 'c:\sas\';

data p.kursy;
	infile 'c:\sas\kursy.txt' delimiter = ',' firstobs = 2;
	input name $ date yymmdd10. open high low close vol;
	format date ddmmyy10.;
run;
