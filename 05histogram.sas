proc sql;
create table p.daily_change as
	select k.*, prev.close as prev_close
	from p.kursy k left join p.kursy prev on k.date = prev.date + 1
	where k.name = prev.name or prev.name is null;
quit;

data p.daily_change;
	set p.daily_change;
	change = (close-prev_close)/prev_close;
	where prev_close; /*odrzuca brakujace*/
run;

proc univariate data=p.daily_change noprint;
	histogram change / endpoints = -0.5 to 0.5  by 0.1  outhist = p.histogram grid cfill = blue;
	title 'Histogram zmian dziennych';
run;
